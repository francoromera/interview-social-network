import React from 'react';
import { Link } from 'react-router-dom';

const User = ({ user }) => {
  return (
    <div className="User">
      <Link to={`/user/${user._id}`}>
        {user.firstName} {user.lastName}
      </Link>
    </div>
  );
};

export default User;
