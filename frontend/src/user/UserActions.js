import * as constants from '../app/constants';

export const set = (users) => {
  return { type: constants.USER_SET, payload: { users }};
};

export const add = (user) => {
  return { type: constants.USER_ADD, payload: { user }};
};

export const login = (user) => {
  return { type: constants.USER_LOGIN, payload: { user }};
};

export const logout = () => {
  return { type: constants.USER_LOGOUT };
};

export const addFollower = (userId) => {
  return { type: constants.USER_ADD_FOLLOWER, payload: { userId }};
};
