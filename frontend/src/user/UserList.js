import React, { Component } from 'react';
import User from '../user/User';
import * as api from '../helpers/api';

class UserList extends Component {

  componentDidMount() {
    api.get(api.urls.USER_LIST).then((users) => {
      this.props.setUsers(users);
    });
  }

  render() {
    return (
      <React.Fragment>
        <h2>Users</h2>
        <ul className="list">
          {this.props.users.map((user) => (
            <li key={user._id}>
              <User user={user} />
            </li>
          ))}
        </ul>
      </React.Fragment>
    );
  }
}

export default UserList;
