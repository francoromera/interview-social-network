import { connect } from 'react-redux';
import UserList from './UserList';
import * as userActions from './UserActions';

const mapStateToProps = (state) => {
  return {
    users: state.users
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUsers: (users) => {
      dispatch(userActions.set(users));
    }
  };
};

const UserListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(UserList);

export default UserListContainer;
