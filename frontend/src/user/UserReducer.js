import * as constants from '../app/constants';

export const users = (state = [], action) => {
  switch (action.type) {
    case constants.USER_SET:
      return [...action.payload.users];
    case constants.USER_ADD:
      return [...state, action.payload.user];
    default:
      return state;
  }
};

export const currentUser = (state = null, action) => {
  switch (action.type) {
    case constants.USER_LOGIN:
      return { ...action.payload.user };
    case constants.USER_ADD_FOLLOWER:
      return { ...state, followers: [...state.followers, action.payload.userId] };
    case constants.USER_LOGOUT:
      return null;
    default:
      return state;
  }
};

export const isLoggedIn = (state = false, action) => {
  switch (action.type) {
    case constants.USER_LOGIN:
      return !!action.payload.user;
    case constants.USER_LOGOUT:
      return false;
    default:
      return state;
  }
};
