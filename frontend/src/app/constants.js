export const USER_SET = 'USER_SET';
export const USER_ADD = 'USER_ADD';
export const USER_LOGIN = 'USER_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';
export const USER_ADD_FOLLOWER = 'USER_ADD_FOLLOWER';

export const PHOTO_SET = 'PHOTO_SET';
export const PHOTO_LIKE = 'PHOTO_LIKE';
export const PHOTO_ADD_COMMENT = 'PHOTO_ADD_COMMENT';

export const COMMENT_SET = 'COMMENT_SET';
