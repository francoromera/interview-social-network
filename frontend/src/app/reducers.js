import { combineReducers } from 'redux';
import * as comment from '../comment/CommentReducer';
import * as photo from '../photo/PhotoReducer';
import * as user from '../user/UserReducer';

const allReducers = Object.assign(
  {},
  comment,
  photo,
  user
);

export default combineReducers(allReducers);
