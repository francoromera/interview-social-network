import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import UserListContainer from '../user/UserListContainer';
import PhotoListContainer from '../photo/PhotoListContainer';
import PhotoDetails from '../photo/PhotoDetails';
import '../assets/App.css';

const App = () => {
  return (
    <div className="App">
      <h1>My Awesome Social Network</h1>
      <Router>
        <React.Fragment>
          <Route exact path="/" component={UserListContainer} />
          <Route exact path="/user/:id" component={PhotoListContainer} />
          <Route exact path="/user/:id/photo/:photoId" component={PhotoDetails} />
        </React.Fragment>
      </Router>
    </div>
  );
};

export default App;
