import * as constants from '../app/constants';

export const set = (comments) => {
  return { type: constants.COMMENT_SET, payload: { comments }};
};
