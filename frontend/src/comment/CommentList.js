import React, { Component } from 'react';
import Comment from './Comment';
import * as api from '../helpers/api';

class CommentList extends Component {

  componentDidMount() {
    api.get(api.urls.COMMENT_LIST, {
      userId: this.props.userId,
      photoId: this.props.photoId
    }).then((comments) => {
      this.props.setComments(comments);
    });
  }

  render() {
    return (
      <React.Fragment>
        <h2>Comments:</h2>
        {
          !this.props.comments.length
            ? <p><i>No comments</i></p>
            : <ul className="list">
                {this.props.comments.map((comment) => (
                  <li key={comment._id}>
                    <Comment comment={comment} />
                  </li>
                ))}
              </ul>
        }
      </React.Fragment>
    );
  }
}

export default CommentList;
