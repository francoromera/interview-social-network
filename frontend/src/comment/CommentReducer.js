import * as constants from '../app/constants';

export const comments = (state = [], action) => {
  switch (action.type) {
    case constants.COMMENT_SET:
      return [...action.payload.comments];
    default:
      return state;
  }
};
