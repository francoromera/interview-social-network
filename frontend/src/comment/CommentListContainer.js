import { connect } from 'react-redux';
import CommentList from './CommentList';
import * as commentActions from './CommentActions';

const mapStateToProps = (state, props) => {
  return {
    photoId: props.photoId,
    userId: props.userId,
    comments: state.comments
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setComments: (comments) => {
      dispatch(commentActions.set(comments));
    }
  };
};

const CommentListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(CommentList);

export default CommentListContainer;
