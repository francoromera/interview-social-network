import React from 'react';
import '../assets/Comment.css';

const Comment = (props) => {

  const { comment } = props;
  const date = new Date(comment.date);

  return (
    <div className="Comment">
      <span>{ date.toLocaleString() }</span>
      { comment.description }
    </div>
  );

};

export default Comment;
