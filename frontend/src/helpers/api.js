const baseUrl = 'http://localhost:3001';

export const urls = {
  USER_LIST: 'users',
  PHOTO_LIST: 'photos/user/:userId',
  PHOTO_GET: 'photos/:photoId',
  COMMENT_LIST: 'photos/:photoId/comments'
};

const buildUrl = (name, params) => {
  let url = `${baseUrl}/${name}`;
  if (params) {
    Object.keys(params).forEach((param) => {
      url = url.replace(`:${param}`, params[param]);
    });
  }
  return url;
};

export const get = (name, params) => {
  const url = buildUrl(name, params);
  return new Promise((resolve, reject) => {
    fetch(url).then(response => response.json()).then(result => {
      if (result && result.message === 'Success') {
        resolve(result.data);
      } else {
        reject((result || { message: 'Error' }).message);
      }
    });
  });
};
