import * as constants from '../app/constants';

export const photos = (state = [], action) => {
  let photo = null;
  if (action.payload && action.payload.id) {
    photo = state.find(it => it.id === action.payload.id);
  }
  const newState = [...state];
  switch (action.type) {
    case constants.PHOTO_SET:
      return action.payload.photos;
    case constants.PHOTO_LIKE:
      newState[state.indexOf(photo)].likes = photo.likes + 1;
      return newState;
    case constants.PHOTO_ADD_COMMENT:
      newState[state.indexOf(photo)].comments = [...photo.comments, action.payload.comment];
      return newState;
    default:
      return state;
  }
};
