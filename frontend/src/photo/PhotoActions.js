import * as constants from '../app/constants';

export const set = (photos) => {
  return { type: constants.PHOTO_SET, payload: { photos }};
};

export const like = (photoId) => {
  return { type: constants.PHOTO_LIKE, payload: { photoId }};
};

export const addComment = (photoId, comment) => {
  return { type: constants.PHOTO_ADD_COMMENT, payload: { photoId, comment }};
};
