import React from 'react';

const Photo = (props) => {

  const { photo } = props;

  return (
    <React.Fragment>
      { photo ? <img src={photo.url} alt={photo._id} /> : '' }
    </React.Fragment>
  );

};

export default Photo;
