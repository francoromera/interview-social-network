import React from 'react';
import { Link } from 'react-router-dom';
import Photo from './Photo';
import '../assets/PhotoItem.css';

const PhotoItem = (props) => {

  const { photo, userId } = props;

  return (
    <Link to={`/user/${userId}/photo/${photo._id}`} className="PhotoItem">
      <Photo key={photo._id} photo={photo} />
    </Link>
  );

};

export default PhotoItem;
