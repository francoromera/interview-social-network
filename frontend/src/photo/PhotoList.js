import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PhotoItem from './PhotoItem';
import * as api from '../helpers/api';

class PhotoList extends Component {

  componentDidMount() {
    api.get(api.urls.PHOTO_LIST, {
      userId: this.props.userId
    }).then((photos) => {
      this.props.setPhotos(photos);
    });
  }

  render() {
    return (
      <React.Fragment>
        <h2>Photos</h2>
        <div>
          {this.props.photos.map((photo) => (
            <PhotoItem key={photo._id} userId={this.props.userId} photo={photo} />
          ))}
        </div>
        <Link to="/" className="button">
          Back to Users
        </Link>
      </React.Fragment>
    );
  }
}

export default PhotoList;
