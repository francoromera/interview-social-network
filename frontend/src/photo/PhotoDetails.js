import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Photo from './Photo';
import CommentListContainer from '../comment/CommentListContainer';
import * as api from '../helpers/api';
import '../assets/PhotoDetails.css';

class PhotoDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      userId: props.match.params.id,
      photoId: props.match.params.photoId,
      photo: null
    };
  }

  componentDidMount() {
    api.get(api.urls.PHOTO_GET, {
      photoId: this.state.photoId
    }).then((photo) => {
      this.setState({ photo });
    });
  }

  render() {
    return (
      <div className="PhotoDetails">
        <h2>{this.state.photo ? this.state.photo.description : ''}</h2>
        <Photo photo={this.state.photo} />
        <CommentListContainer userId={this.state.userId} photoId={this.state.photoId} />
        <Link to={`/user/${this.state.userId}`} className="button">
          Back to Photos
        </Link>
      </div>
    );
  }
}

export default PhotoDetails;
