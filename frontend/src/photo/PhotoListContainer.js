import { connect } from 'react-redux';
import PhotoList from './PhotoList';
import * as photoActions from './PhotoActions';

const mapStateToProps = (state, props) => {
  return {
    photos: state.photos,
    userId: props.match.params.id
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPhotos: (photos) => {
      dispatch(photoActions.set(photos));
    }
  };
};

const PhotoListContainer = connect(
    mapStateToProps,
    mapDispatchToProps,
)(PhotoList);

export default PhotoListContainer;
