const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const users = await req.userCollection.find({});
    res.json({ message: 'Success', data: users });
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

router.post('/follow', async (req, res) => {
  try {
    if (req.body.user && req.body.follow) {
      const user = await req.userCollection.findOne({ email: req.body.user });
      const follow = await req.userCollection.findOne({ email: req.body.follow });
      if (user && follow) {
        await req.userCollection.update({ _id: user._id }, {
          $addToSet: { following: follow._id }
        });
        await req.userCollection.update({ _id: follow._id }, {
          $addToSet: { followers: user._id }
        });
        res.json({ message: 'Success', data: { user: user._id, follow: follow._id }});
      } else {
        res.status(404).json({ error: 'User not found' });
      }
    } else {
      res.status(400).json({ error: 'Missing arguments' });
    }
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

module.exports = router;
