const express = require('express');
const monk = require('monk');
const router = express.Router();

router.get('/user/:userId', async (req, res) => {
  try {
    const photos = await req.photoCollection.find({ user: monk.id(Number(req.params.userId)) });
    res.json({ message: 'Success', data: photos });
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

router.get('/:photoId', async (req, res) => {
  try {
    const photo = await req.photoCollection.findOne({ _id: monk.id(Number(req.params.photoId)) });
    res.json({ message: 'Success', data: photo });
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

router.get('/:photoId/comments', async (req, res) => {
  try {
    const comments = await req.commentCollection.find({ photo: monk.id(Number(req.params.photoId)) });
    res.json({ message: 'Success', data: comments });
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

router.post('/like', async (req, res) => {
  try {
    if (req.body.user && req.body.photoId) {
      const user = await req.userCollection.findOne({ email: req.body.user });
      const photo = await req.photoCollection.findOne({ _id: monk.id(req.body.photoId) });
      if (user && photo) {
        await req.photoCollection.update({ _id: photo._id }, {
          $inc: { likes: 1 }
        });
        res.json({ message: 'Success' });
      } else {
        res.status(404).json({ error: 'User or photo not found' });
      }
    } else {
      res.status(400).json({ error: 'Missing arguments' });
    }
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

router.post('/comment', async (req, res) => {
  try {
    if (req.body.user && req.body.photoId && req.body.description) {
      const user = await req.userCollection.findOne({ email: req.body.user });
      const photo = await req.photoCollection.findOne({ _id: monk.id(req.body.photoId) });
      if (user && photo) {
        await req.photoCollection.update({ _id: photo._id }, {
          $inc: { comments: 1 }
        });
        await req.commentCollection.insert({
          photo: photo._id,
          user: user._id,
          date: new Date(),
          description: req.body.description
        });
        res.json({ message: 'Success' });
      } else {
        res.status(404).json({ error: 'User or photo not found' });
      }
    } else {
      res.status(400).json({ error: 'Missing arguments' });
    }
  } catch (e) {
    res.status(500).json({ error: e });
  }
});

module.exports = router;
