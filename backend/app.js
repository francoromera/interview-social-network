const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const photosRouter = require('./routes/photos');
const dbHelper = require('./helpers/db');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

const monk = require('monk');
const db = monk('localhost:27017/socialnetwork');

app.use((req, res, next) => {
  req.db = db;
  req.userCollection = db.get('users');
  req.photoCollection = db.get('photos');
  req.commentCollection = db.get('comments');
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/photos', photosRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  res.status(err.status || 500).json({ error: err.message });
});

dbHelper.populate(db).then(() => {
  console.log('Server ready');
});

module.exports = app;