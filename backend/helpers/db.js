const monk = require('monk');
const bcrypt = require('bcrypt');

const populate = async (db) => {

  const pass1 = await bcrypt.hash('123456', 12);
  const pass2 = await bcrypt.hash('654321', 12);

  // setup users
  const userCollection = db.get('users');
  await userCollection.drop();
  await userCollection.createIndex('email');

  // setup photos
  const photoCollection = db.get('photos');
  await photoCollection.drop();
  await userCollection.createIndex('user');

  // setup comment
  const commentCollection = db.get('comments');
  await commentCollection.drop();
  await userCollection.createIndex('photo');

  // ids
  const userId1 = monk.id(1);
  const userId2 = monk.id(2);
  const photoId1 = monk.id(3);
  const photoId2 = monk.id(4);
  const photoId3 = monk.id(5);
  const photoId4 = monk.id(6);

  // users
  await userCollection.insert([{
    _id: userId1,
    email: 'francoromera@gmail.com',
    password: pass1,
    nickName: 'francoromera',
    firstName: 'Franco',
    lastName: 'Romera',
    following: [],
    followers: []
  }, {
    _id: userId2,
    email: 'johnsmith@hotmail.com',
    password: pass2,
    nickName: 'John',
    firstName: 'John',
    lastName: 'Smith',
    following: [],
    followers: []
  }]);

  // photos
  await photoCollection.insert([{
    _id: photoId1,
    added: new Date('01/01/2001'),
    likes: 0,
    comments: 2,
    url: 'https://homepages.cae.wisc.edu/~ece533/images/cat.png',
    description: 'My cat is about to suffer',
    user: userId1
  }, {
    _id: photoId2,
    added: new Date('01/01/2002'),
    likes: 0,
    comments: 0,
    url: 'https://homepages.cae.wisc.edu/~ece533/images/fruits.png',
    description: 'Ready to eat!!!',
    user: userId1
  }, {
    _id: photoId3,
    added: new Date('01/01/2003'),
    likes: 2,
    comments: 0,
    url: 'https://homepages.cae.wisc.edu/~ece533/images/mountain.png',
    description: 'Ready to climb!!!',
    user: userId1
  }, {
    _id: photoId4,
    added: new Date('01/01/2004'),
    likes: 1234,
    comments: 1,
    url: 'https://homepages.cae.wisc.edu/~ece533/images/pool.png',
    description: 'Ready to play!!!',
    user: userId2
  }]);

  // comments
  await commentCollection.insert([{
    photo: photoId1,
    user: userId2,
    date: new Date(),
    description: 'Wow, nice pic'
  }, {
    photo: photoId1,
    user: userId2,
    date: new Date(),
    description: 'Congrats!'
  }, {
    photo: photoId1,
    user: userId2,
    date: new Date(),
    description: 'Hello World!'
  }, {
    photo: photoId4,
    user: userId1,
    date: new Date(),
    description: 'This is a meaningless comment'
  }]);

};

module.exports = {
  populate
};