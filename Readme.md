# Interview: My awesome social network

## Setup

### Backend

```
cd backend
npm install
mkdir data
/Library/Mongo/bin/mongod --dbpath ./data
PORT=3001 npm run start
```

### Frontend

```
cd frontend
npm install
npm run start
```
